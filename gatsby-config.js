module.exports = {
  siteMetadata: {
    title: `Card`,
    description: `Dagcoin Card`,
    author: `@primefunnel`,
    siteUrl: `https://gift-carddagcoin.netlify.app/`,
  },
  plugins: [
    `gatsby-plugin-google-tagmanager`,
    {
      resolve: "gatsby-plugin-google-tagmanager",
      options: {
        id: "GTM-P3NFMHX",
        // Include GTM in development.
        // Defaults to false meaning GTM will only be loaded in production.
        includeInDevelopment: false,
        // datalayer to be set before GTM is loaded
        // should be an object or a function that is executed in the browser
        // Defaults to null
        defaultDataLayer: { platform: "gatsby" },
      },
    },
    `gatsby-plugin-modal-routing`,
      {
          resolve: `gatsby-plugin-modal-routing`,
      },
    `gatsby-plugin-sass`,
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `./src/images`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
      {
        resolve: `gatsby-plugin-sitemap`,
        options: {
          output:'/sitemap.xml'
        }
      },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/gatsby-icon.png`, // This path is relative to the root of the site.
      },
    },
    {
      resolve: `gatsby-source-wordpress`,
      options: {
        baseUrl: `giftcard.primefunnel.com`,
        protocol: `https`,
        hostingWPCOM: false,
        useACF: true
      }
    },
    `gatsby-plugin-offline`,
    {
      resolve: "gatsby-plugin-react-svg",
      options: {
        rule: {
          include: /\.inline\.svg$/
        }
      }
    },
    {
      resolve: 'gatsby-background-image',
      options: {
        specialChars: '/:',
      },
    },
    `gatsby-plugin-netlify`
  ],
}
