

const path = require(`path`)

const languages = [
  {
    path: "/",
    code: "en_US",
    mainLanguage: true
  },
  {
    path: "/ru",
    code: "ru_RU",
    mainLanguage: false
  },
  {
    path: "/es",
    code: "es_ES",
    mainLanguage: false
  },
  {
    path: "/ar",
    code: "ar",
    mainLanguage: false
  },
]

exports.createPages = async ({ actions: { createPage } }) => {
  const HomepageTemplate = path.resolve("./src/templates/Homepage.js")
  const TermsTemplate = path.resolve("./src/templates/Terms.js")
  const PrivacyTemplate = path.resolve("./src/templates/Privacy.js")
  const SupportTemplate = path.resolve("./src/templates/Support.js")
  
  languages.forEach(lang => {
    createPage({
      path: lang.path,
      component: HomepageTemplate,
      context: {
        lang: lang.code,
        currentPath: '/'
      }
    })
    createPage({
      path: lang.mainLanguage ? lang.path + 'terms-of-service' : lang.path + '/terms-of-service',
      component: TermsTemplate,
      context: {
        lang: lang.code,
        currentPath: '/terms-of-service'
      }
    })
    createPage({
      path: lang.mainLanguage ? lang.path + 'privacy-policy' : lang.path + '/privacy-policy',
      component: PrivacyTemplate,
      context: {
        lang: lang.code,
        currentPath: '/privacy-policy'
      }
    })
    createPage({
      path: lang.mainLanguage ? lang.path + 'support' : lang.path + '/support',
      component: SupportTemplate,
      context: {
        lang: lang.code,
        currentPath: '/support'
      }
    })
  })
}